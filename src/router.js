import Vue from 'vue'
import Router from 'vue-router'
import home from '@/views/home'
import career from '@/views/career'
import portfolio from '@/views/portfolio'
import product from '@/views/product'
import blog from '@/views/blog'
import blogPost from '@/views/blog-post'
import contact from '@/views/contact'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/career/',
      name: 'career',
      component: career,
      menuItem: true
    },
    {
      path: '/portfolio/',
      name: 'portfolio',
      component: portfolio,
      menuItem: true
    },
    {
      path: '/portfolio/:slug',
      name: 'product',
      component: product
    },
    {
      path: '/blog/',
      name: 'blog',
      component: blog,
      menuItem: true
    },
    {
      path: '/blog/:slug',
      name: 'blog-post',
      component: blogPost
    },
    {
      path: '/contact/',
      name: 'contact',
      component: contact,
      menuItem: true
    }
  ]
})
