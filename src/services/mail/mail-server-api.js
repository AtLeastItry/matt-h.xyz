import axios from 'axios'
import { ApplyAuthenticationHeader, AuthResponseFailureInterceptor, DefaultResponseInterceptor } from '@/services/mail/interceptors';

export default class MailServerApi {
    constructor() {
        this._urlBase = "https://mailserverapi.azurewebsites.net/api/"

        this._config = {
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json',
            },
            validateStatus: (status) => {
                return status >= 200 && status < 300;
            }
        }

        axios.interceptors.request.use(ApplyAuthenticationHeader);
        axios.interceptors.response.use(DefaultResponseInterceptor, AuthResponseFailureInterceptor);
    }

    sendMailAsync = async (email) => {
        let url = this._urlBase + 'sendgrid/send';
        const response = await axios.post(url, email, this._config);
        
        return response.data;
    }
}