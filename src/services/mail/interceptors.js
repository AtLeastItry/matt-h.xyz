import axios, { AxiosResponse, AxiosRequestConfig } from 'axios';
import Router from '@/router';

export async function DefaultResponseInterceptor(response) {
    return response;
}

export async function AuthResponseFailureInterceptor(error) {
    if (error.response && error.response.status === 401) {
        //do something
    }

    return error;
}

export function ApplyAuthenticationHeader(config) {
    config.headers['ApiKey'] = "MS.ZoNzbZLUYEqqRRaVh!5zVc#8xdRww&!9M7JAqSlBU4J6uxiWP57kaix5DPRhZX#U";

    return config;
}